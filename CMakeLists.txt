tdaq_package()

tdaq_add_library(omnipymodule 
  modules/*.cc
  INCLUDE_DIRECTORIES PRIVATE omni/include/omniORB4/internal modules include ${TDAQ_INST_PATH}/include/omniORB4/internal
  OPTIONS PRIVATE $<$<CXX_COMPILER_ID:GNU>:-fpermissive>
  DEFINITIONS 
     -DOMNIPY_MAJOR=4 -DOMNIPY_MINOR=2 -DOMNIPY_VERSION_STRING=\"4.2.2\"
     -D__linux__ -D__x86__
     -D__OMNIORB4__
     -DPYTHON_INCLUDE=<Python.h> -DPYTHON_THREAD_INC=<pythread.h>
  LINK_LIBRARIES Python::Development omniORB4 omnithread)
set_target_properties(omnipymodule PROPERTIES LIBRARY_OUTPUT_NAME _omnipymodule PREFIX "")

tdaq_add_python_files(omniidl_be/python.py DESTINATION omniidl_be)
tdaq_add_python_package(omniORB)
